//
//  HomeView.swift
//  Pribit_Test
//
//  Created by Kedar Sukerkar on 05/06/23.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: .leading, spacing: 10) {
                    ImagePostView()
                    ReplyPostView()
                }
            }
            .preferredColorScheme(.light)
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    HStack {
                        Image(systemName: "chevron.left")
                            .font(.headline)
                        Spacer()
                        Text("자유톡")
                            .font(.notoSans(.bold, size: 18))
                            .foregroundColor(.titleColor)
                        Spacer()
                        Image("bell")
                    }
                }
            }
        }
    }
}






struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
