//
//  Font+Extension.swift
//  Pribit_Test
//
//  Created by Kedar Sukerkar on 05/06/23.
//

import SwiftUI

extension Font {
    
    enum NotoSansFont {
        case bold
        case regular
        case medium
        
        var value: String {
            switch self {
                case .bold:
                    return "NotoSans-Bold"
                case .regular:
                    return "NotoSans-Regular"
                case .medium:
                    return "NotoSans-Medium"
            }
        }
    }
    
    enum RobotoFont {
        case regular
        case medium
        
        var value: String {
            switch self {
                case .regular:
                    return "Roboto-Regular"
                case .medium:
                    return "Roboto-Medium"
            }
        }
    }

    
    static func notoSans(_ type: NotoSansFont, size: CGFloat = 26) -> Font {
        return .custom(type.value, size: size)
    }
    
    static func roboto(_ type: RobotoFont, size: CGFloat = 26) -> Font {
        return .custom(type.value, size: size)
    }
}
