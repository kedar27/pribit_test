//
//  Color+Extension.swift
//  Pribit_Test
//
//  Created by Kedar Sukerkar on 05/06/23.
//

import SwiftUI

extension Color {
    public static let titleColor = Color(UIColor(red: 29/255.0, green: 35/255.0, blue: 43/255.0, alpha: 1.0))
    public static let subTitleColor = Color(UIColor(red: 145/255.0, green: 158/255.0, blue: 182/255.0, alpha: 1.0))
    public static let primaryTextColor = Color(UIColor(red: 49/255.0, green: 59/255.0, blue: 73/255.0, alpha: 1.0))
    public static let primaryCTAColor = Color(UIColor(red: 1/255.0, green: 185/255.0, blue: 159/255.0, alpha: 1.0))
    public static let tagBGColor = Color(UIColor(red: 247/255.0, green: 248/255.0, blue: 250/255.0, alpha: 1.0))
    public static let tagTextColor = Color(UIColor(red: 90/255.0, green: 107/255.0, blue: 135/255.0, alpha: 1.0))
    public static let tagBorderColor = Color(UIColor(red: 206/255.0, green: 211/255.0, blue: 222/255.0, alpha: 1.0))
    public static let textFieldTextColor = Color(UIColor(red: 175/255.0, green: 185/255.0, blue: 202/255.0, alpha: 1.0))
    public static let viewBorderColor = Color(UIColor(red: 247/255.0, green: 248/255.0, blue: 250/255.0, alpha: 1.0))

}
