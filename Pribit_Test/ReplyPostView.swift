//
//  ReplyPostView.swift
//  Pribit_Test
//
//  Created by Kedar Sukerkar on 05/06/23.
//

import SwiftUI

struct ReplyPostView: View {
    
    // MARK: - Properties
    
    var post = """
어머 제가 있던 테이블이 제일 반응이 좋았나보네요🤭
우짤래미님도 아시겠지만 저도 일반인 몸매 그 이상도 이하도
아니잖아요?! 그런 제가 기꺼이 도전해봤는데 생각보다
괜찮았어요! 오늘 중으로 라이브 리뷰 올라온다고 하니
꼭 봐주세용~!
"""
    
    let horizontalPadding: CGFloat = 16
    
    // MARK: - Body
    
    var body: some View {
        VStack(alignment: .leadingTitle) {
            HStack(alignment: .center) {
                Image("avatar")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .background(Color(red: 250/255.0, green: 222/255.0, blue: 175/255.0))
                    .clipShape(Circle())
                    .frame(width: 38, height: 38)
                
                HStack(spacing: 4) {
                    Text("안녕 나 응애")
                        .font(.notoSans(.bold, size: 14))
                        .foregroundColor(.titleColor)
                        .alignmentGuide(.leadingTitle, computeValue: { $0[.leading]})
                    
                    Image("expertIcon")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 14, height: 14)
                    
                    Text("1일전")
                        .font(.notoSans(.medium, size: 10))
                        .foregroundColor(.subTitleColor)
                }
                
                Spacer()
                
                Image(systemName: "ellipsis")
                    .foregroundColor(Color.subTitleColor)
                    .padding(.leading, 30)
            }
            .padding(.horizontal, horizontalPadding)
            
            Text(post)
                .font(.notoSans(.medium, size: 12))
                .foregroundColor(.primaryTextColor)
                .alignmentGuide(.leadingTitle, computeValue: { $0[.leading]})
            
            HStack(spacing: 15.0) {
                Label("5", image: "heart")
                    .foregroundColor(Color.subTitleColor)
                    .font(.roboto(.regular, size: 12))
                
                Label("5", image: "talk")
                    .foregroundColor(Color.subTitleColor)
                    .font(.roboto(.regular, size: 12))
            }
            .frame(width: 85 ,height: 13)
            
            HStack(alignment: .center, spacing: 6) {
                Image("avatar2")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .background(Color(red: 240/255.0, green: 179/255.0, blue: 156/255.0))
                    .clipShape(Circle())
                    .frame(width: 38, height: 38)
                
                Text("ㅇㅅㅇ")
                    .font(.notoSans(.bold, size: 14))
                    .foregroundColor(.titleColor)
                
                Image("expertIcon")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 14, height: 14)
                
                Text("1일전")
                    .font(.notoSans(.medium, size: 10))
                    .foregroundColor(.subTitleColor)
                
                
                Image(systemName: "ellipsis")
                    .foregroundColor(Color.subTitleColor)
                    .offset(CGSize(width: (UIScreen.main.bounds.width - 32 - 38 - 4 - 158) , height: 0))
                
            }
            Group {
                Text("오 대박! 라이브 리뷰 오늘 올라온대요? 챙겨봐야겠다")
                    .font(.notoSans(.regular, size: 12))
                    .foregroundColor(
                        Color(red: 49/255.0, green: 59/255.0, blue: 73/255.0)
                    )
                
                Label("5", image: "heart")
                    .foregroundColor(Color.subTitleColor)
                    .font(.roboto(.regular, size: 12))
                    .frame(width: 35 ,height: 13)
            }
            .alignmentGuide(.leadingTitle) { _ in -44 }
        }
        
        Divider()
            .frame(height: 2)
            .overlay(Color.viewBorderColor)
        
        HStack {
            Image("placeholder")
            
            Text("댓글을 남겨주세요.")
                .font(.notoSans(.regular, size: 12))
                .foregroundColor(.textFieldTextColor)
            
            Spacer()
            
            Text("등록")
                .font(.notoSans(.medium, size: 12))
                .foregroundColor(.subTitleColor)
        }
        .padding(.horizontal, horizontalPadding)
        
        Divider()
            .frame(height: 2)
            .overlay(Color.viewBorderColor)
    }
}

extension HorizontalAlignment {
    struct LeadingTitle: AlignmentID {
        static func defaultValue(in d: ViewDimensions) -> CGFloat {
            d[.leading]
        }
    }
    
    static let leadingTitle = HorizontalAlignment(LeadingTitle.self)
}
