//
//  Pribit_TestApp.swift
//  Pribit_Test
//
//  Created by Kedar Sukerkar on 05/06/23.
//

import SwiftUI

@main
struct Pribit_TestApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
