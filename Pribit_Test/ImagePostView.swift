//
//  ImagePostView.swift
//  Pribit_Test
//
//  Created by Kedar Sukerkar on 05/06/23.
//

import SwiftUI

struct ImagePostView: View {
    
    // MARK: - Properties
    
    
    let tags = ["#2023" , "#TODAYISMONDAY" , "#TOP" ,"#POPS!" , "#WOW" , "#ROW"]
    let imageURL = URL(string: "https://wjddnjs754.cafe24.com/web/product/small/202303/5b9279582db2a92beb8db29fa1512ee9.jpg")
    let horizontalPadding: CGFloat = 16
    
    // MARK: - Body
    
    
    var body: some View {
        Group {
            Spacer()
                .frame(height: 10)
            
            HStack {
                Image("avatar")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .clipShape(Circle())
                    .frame(width: 38, height: 38)
                
                VStack(alignment: .leading, spacing: 0) {
                    HStack(spacing: 4) {
                        Text("안녕 나 응애")
                            .font(.notoSans(.bold, size: 14))
                            .foregroundColor(.titleColor)
                        
                        Image("expertIcon")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 14, height: 14)
                        
                        Text("1일전")
                            .font(.notoSans(.medium, size: 10))
                            .foregroundColor(.subTitleColor)
                    }
                    
                    Text("165cm • 135Kg")
                        .font(.roboto(.regular, size: 12))
                        .foregroundColor(.subTitleColor)
                }
                Spacer()
                
                Text("팔로우")
                    .font(.notoSans(.medium, size: 12))
                    .foregroundColor(.white)
                    .frame(width: 58, height: 24)
                    .background(Color.primaryCTAColor)
                    .clipShape(Capsule())
                
                
            }
            .padding(.horizontal, horizontalPadding)
            
            Text("지난 월요일에 했던 이벤트 중 가장 괜찮은 상품 뭐야?")
                .font(.notoSans(.bold, size: 14))
                .foregroundColor(.titleColor)
                .padding(.horizontal, horizontalPadding)
            
            Text("""
                         "지난 월요일에 2023년 S/S 트렌드 알아보기 이벤트 참석했던 팝들아~
                         혹시 어떤 상품이 제일 괜찮았어?
                         
                         후기 올라오는거 보면 로우라이즈? 그게 제일 반응 좋고 그 테이블이
                         제일 재밌었다던데 맞아???
                         
                         올해 로우라이즈가 트렌드라길래 나도 도전해보고 싶은데 말라깽이가
                         아닌 사람들도 잘 어울릴지 너무너무 궁금해ㅜㅜ로우라이즈 테이블에
                         있었던 팝들 있으면 어땠는지 후기 좀 공유해주라~~!
                         """)
            .font(.notoSans(.medium, size: 12))
            .foregroundColor(.primaryTextColor)
            .padding(.horizontal, horizontalPadding)
            
            GeometryReader { geometry in
                TagLayoutView(
                    tags,
                    tagFont: UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.thin),
                    padding: 20,
                    parentWidth: geometry.size.width) { tag in
                        Text(tag)
                            .font(.roboto(.medium, size: 12))
                            .fixedSize()
                            .padding(EdgeInsets(top: 5, leading: 12, bottom: 5, trailing: 12))
                            .foregroundColor(Color.tagTextColor)
                            .background(
                                Capsule(style: .continuous)
                                    .stroke(Color.tagBorderColor, style: StrokeStyle(lineWidth: 0.5))
                                    .background(Capsule().fill(Color.tagBGColor))
                            )
                    }.padding(.horizontal, 20)
            }
            .frame(height: 66)
            
            AsyncImage(url: imageURL) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .clipped()
                    .cornerRadius(12)
                
            } placeholder: {
                ProgressView()
                    .frame(alignment: .center)
            }
            .frame(maxWidth: .infinity)
            .frame(height: 450, alignment: .center)
        }
        
        HStack(spacing: 15.0) {
            Label("5", image: "heart")
                .foregroundColor(Color.subTitleColor)
            Label("5", image: "talk")
                .foregroundColor(Color.subTitleColor)
            Image("bookmark")
                .resizable()
                .frame(width: 14,height: 17)
            
            Image(systemName: "ellipsis")
                .foregroundColor(Color.subTitleColor)
                .padding(.leading, 30)
        }
        .font(.roboto(.regular, size: 12))
        .padding(.horizontal, horizontalPadding)
        
        Divider()
            .frame(height: 2)
            .overlay(Color.viewBorderColor)
    }
}
